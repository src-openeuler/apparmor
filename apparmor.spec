Name:           apparmor
Version:        2.13.4
Release:        2
Summary:        AppArmor userlevel utility
License:        GPL v2.0 AND LGPL v2.1 or later
URL:            https://gitlab.com/apparmor/apparmor
Source0:        https://gitlab.com/apparmor/apparmor/-/archive/v%{version}/apparmor-v%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  bison dejagnu flex gcc-c++ pcre-devel pkg-config gdb
BuildRequires:  python3 python3-pyflakes python3-devel
BuildRequires:  perl(Locale::gettext) perl-devel
BuildRequires:  swig ruby-devel libtool
BuildRequires:  libstdc++-static
Requires:       perl perl-apparmor = %{version}-%{release}
Requires:       python3 python3-apparmor = %{version}-%{release}

%description
This package provides AppArmor Parser, abstractions, profiles, utils and docs.

%package libs
Summary:        Libs of AppArmor
License:        LGPL v2.1 or later

%description libs
This package provides the libapparmor libraries.

%package devel
Summary:        Development headers and libraries
Requires:       %{name}-libs = %{version}-%{release}

%description devel
These headers and libraries are needed for developing.

%package -n perl-apparmor
Summary:        Perl interface
License:        GPL v2.0 AND LGPL v2.1 or later
Requires:       %{name}-libs = %{version}-%{release}
Requires:       perl

%description -n perl-apparmor
This package provides the perl interface.

%package -n python3-apparmor
Summary:        Python 3 interface
License:        GPL v2.0 AND LGPL v2.1 or later
Requires:       %{name}-libs = %{version}-%{release}
Requires:       python3

%description -n python3-apparmor
This package provides the python3 interface.

%package -n ruby-apparmor
Summary:        Ruby interface for libapparmor functions
License:        GPL v2.0 AND LGPL v2.1 or later
Requires:       %{name}-libs = %{version}-%{release}
Requires:       ruby = %(rpm -q --qf '%%{version}' ruby)

%description -n ruby-apparmor
This package provides the ruby interface.

%package -n pam_apparmor
Summary:        PAM module for AppArmor
License:        GPL v2.0 AND LGPL v2.1 or later
BuildRequires:  pam-devel
Requires:       pam

%description -n pam_apparmor
Pam related security policy.

%prep
%autosetup -n %{name}-v%{version} -p1

%build
export PYTHON=/usr/bin/python3

(
cd ./libraries/libapparmor
./autogen.sh
%configure --with-perl --with-python --with-ruby
make
)

make -C utils
make -C binutils
make -C parser V=1
make -C changehat/pam_apparmor
make -C profiles

%check
export PYTHON=/usr/bin/python3
export PYTHON_VERSIONS=python3

make check -C libraries/libapparmor
make check -C parser
make check -C binutils

make -C profiles check-parser

make check -C utils

%install
export PYTHON=/usr/bin/python3

%make_install -C libraries/libapparmor
%make_install -C libraries/libapparmor/swig
%make_install -C utils
%make_install -C binutils
%make_install -C profiles
%make_install -C parser
%make_install -C changehat/pam_apparmor SECDIR=%{buildroot}/%{_lib}/security

mkdir -p %{buildroot}%{_localstatedir}/log/apparmor
( cd %{buildroot}/%{_sbindir} && ln -s %{_bindir}/aa-exec exec )
( cd %{buildroot}%{_mandir}/man1 && ln -s aa-exec.1.gz exec.1.gz )

mkdir -p %{buildroot}%{_localstatedir}/lib/apparmor/cache
( cd %{buildroot}%{_sysconfdir}/apparmor.d/ && ln -s ../../%{_localstatedir}/lib/apparmor/cache cache )
mkdir -p %{buildroot}%{_localstatedir}/cache/apparmor
( cd %{buildroot}%{_sysconfdir}/apparmor.d/ && ln -s ../../%{_localstatedir}/cache/apparmor cache.d )

find %{buildroot} -name .packlist -exec rm -vf {} \;
find %{buildroot} -name perllocal.pod -exec rm -vf {} \;
rm -f %{buildroot}%{_mandir}/man8/decode.8
rm -f %{buildroot}/sbin/rcapparmor
rm -f %{buildroot}%{_libdir}/libapparmor.la
rm -f %{buildroot}%{_libdir}/libapparmor.a

mv %{buildroot}/sbin/apparmor_parser %{buildroot}%{_sbindir}/apparmor_parser
mkdir -p %{buildroot}/usr/lib/apparmor
mv %{buildroot}/lib/apparmor/rc.apparmor.functions %{buildroot}%{_prefix}/lib/apparmor/rc.apparmor.functions
mv %{buildroot}/lib/apparmor/apparmor.systemd %{buildroot}%{_prefix}/lib/apparmor/apparmor.systemd

for pkg in apparmor-utils apparmor-parser aa-binutils; do
    %find_lang $pkg
done

%files -f apparmor-parser.lang -f aa-binutils.lang -f apparmor-utils.lang
%defattr(-,root,root)
%doc parser/*.[1-9].html
%doc parser/README
%doc utils/vim/apparmor.vim.5.html
%doc common/apparmor.css
%doc %{_mandir}/man1/*
%doc %{_mandir}/man5/*
%doc %{_mandir}/man7/*
%doc %{_mandir}/man8/*
%doc utils/*.[0-9].html
%doc common/apparmor.css
%license parser/COPYING.GPL

%dir %{_sysconfdir}/apparmor
%dir %{_sysconfdir}/apparmor.d
%{_sysconfdir}/apparmor.d/cache
%{_sysconfdir}/apparmor.d/cache.d
%config(noreplace) %{_sysconfdir}/apparmor/subdomain.conf
%config(noreplace) %{_sysconfdir}/apparmor/parser.conf
%config(noreplace) %{_sysconfdir}/apparmor/easyprof.conf
%config(noreplace) %{_sysconfdir}/apparmor/logprof.conf
%config(noreplace) %{_sysconfdir}/apparmor/notify.conf
%config(noreplace) %{_sysconfdir}/apparmor/severity.db

%{_localstatedir}/lib/apparmor
%{_localstatedir}/cache/apparmor
%{_localstatedir}/log/apparmor

%dir %{_datadir}/apparmor
%{_datadir}/apparmor/apparmor.vim
%{_datadir}/apparmor/easyprof/

%{_sbindir}/*
%{_bindir}/*
%{_prefix}/lib/apparmor/*
%{_unitdir}/apparmor.service

%defattr(644,root,root,755)
%dir %{_sysconfdir}/apparmor.d/abstractions
%config(noreplace) %{_sysconfdir}/apparmor.d/abstractions/*
%dir %{_sysconfdir}/apparmor.d/disable
%dir %{_sysconfdir}/apparmor.d/local
%dir %{_sysconfdir}/apparmor.d/tunables
%dir %{_sysconfdir}/apparmor.d/apache2.d
%config(noreplace) %{_sysconfdir}/apparmor.d/apache2.d/phpsysinfo
%config(noreplace) %{_sysconfdir}/apparmor.d/tunables/*
%config(noreplace) %{_sysconfdir}/apparmor.d/bin.*
%config(noreplace) %{_sysconfdir}/apparmor.d/sbin.*
%config(noreplace) %{_sysconfdir}/apparmor.d/usr.*
%config(noreplace) %{_sysconfdir}/apparmor.d/lsb_release
%config(noreplace) %{_sysconfdir}/apparmor.d/nvidia_modprobe
%config(noreplace) %{_sysconfdir}/apparmor.d/local/*
%dir %{_datadir}/apparmor/
%{_datadir}/apparmor/extra-profiles/

%files libs
%defattr(-,root,root)
%{_libdir}/libapparmor.so.*

%files devel
%defattr(-,root,root)
%doc %{_mandir}/man2/*
%doc %{_mandir}/man3/*
%dir %{_includedir}/aalogparse
%{_includedir}/sys/apparmor.h
%{_includedir}/sys/apparmor_private.h
%{_includedir}/aalogparse/*
%{_libdir}/libapparmor.so
%{_libdir}/pkgconfig/libapparmor.pc


%files -n perl-apparmor
%defattr(-,root,root)
%{perl_vendorarch}/auto/LibAppArmor/
%{perl_vendorarch}/LibAppArmor.pm

%files -n python3-apparmor
%defattr(-,root,root)
%{python3_sitearch}/LibAppArmor-%{version}-py*.egg-info
%dir %{python3_sitearch}/LibAppArmor
%dir %{python3_sitearch}/LibAppArmor/__pycache__
%{python3_sitearch}/LibAppArmor/*
%{python3_sitelib}/apparmor/
%{python3_sitelib}/apparmor-%{version}-py*.egg-info

%files -n ruby-apparmor
%defattr(-,root,root)
%{_prefix}/local/lib64/ruby/site_ruby/LibAppArmor.so

%files -n pam_apparmor
%attr(555,root,root) /%{_lib}/security/pam_apparmor.so

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%changelog
* Mon Sep 14 2020 openEuler Buildteam<buildteam@openeuler.org> - 2.13.4-2
- DESC: fix url.

* Wed Jun 24 2020 steven<steven_ygui@163.com> - 2.13.4-1
- DESC: RPM package initialize.
